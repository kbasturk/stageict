from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.conf.global_settings import LANGUAGES


# Models of StageICT.nl

def user_directory_path(instance, filename):
    return 'static/jobportal/uploads/student/{0}/images/{1}'.format(instance.user.id, filename)


class User(AbstractUser):
    is_company = models.BooleanField(default=False)
    is_student = models.BooleanField(default=False)


class StudentProfile(models.Model):
    objects = User()
    MEN = 'Man'
    WOMEN = 'Vrouw'
    NEUTRAL = 'Neutraal'
    GENDER_CHOICES = [
        (MEN, 'Man'),
        (WOMEN, 'Vrouw'),
        (NEUTRAL, 'Neutraal'),
    ]

    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, related_name='student_profile')
    age = models.IntegerField(null=True)
    gender = models.CharField(max_length=30, choices=GENDER_CHOICES, null=True)
    about = models.TextField(null=True)
    study_year = models.IntegerField(null=True)
    is_available = models.BooleanField(default=True)
    student_image = models.FileField(upload_to=user_directory_path, null=True)  # Achteraf toegevoegd
    phone_number = models.CharField(max_length=30, null=True)  # Achteraf toegevoegd
    date_of_birth = models.DateField(max_length=8, null=True) #Achteraf toegevoegd
    slogan = models.CharField(max_length=30, null=True)
    language = models.CharField(max_length=20, choices=LANGUAGES, null=True)

    def __str__(self):
        return self.user.first_name + ' ' + self.user.last_name


class Interest(models.Model):
    name = models.CharField(max_length=30)
    user = models.ForeignKey(StudentProfile, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


# Function for upload path company image
# File will be uploaded to MEDIA_ROOT/<id>/company_image/<filename>
def user_directory_path(instance, filename):
    return 'static/jobportal/uploads/company/{0}/images/{1}'.format(instance.user.id, filename)
    

class CompanyProfile(models.Model):
    objects = User()
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, related_name='company_profile')
    company_name = models.CharField(max_length=30)
    website = models.CharField(max_length=254, blank=True)
    company_image = models.FileField(upload_to=user_directory_path, blank=True)
    address = models.CharField(max_length=100, null=True)
    zip_code = models.CharField(max_length=10, null=True)
    city = models.CharField(max_length=30, null=True)
    phone_number = models.CharField(max_length=30, null=True)


    def __str__(self):
        return self.company_name


def job_contact_person_path(instance, filename):
    return 'uploads/company/{0}/images/{1}'.format(instance.user.id, filename)


class Job(models.Model):
    objects = CompanyProfile()
    user = models.ForeignKey(CompanyProfile, on_delete=models.CASCADE)
    title = models.CharField(max_length=300)
    description = models.TextField()
    compensation = models.IntegerField()
    last_date = models.DateTimeField(null=True, blank=True)
    contact_person = models.CharField(max_length=30)
    contact_person_image = models.FileField(upload_to=job_contact_person_path, blank=True)
    created_at = models.DateTimeField(default=timezone.now, blank=True)
    is_active = models.BooleanField(default=True)
    address = models.CharField(max_length=100, null=True)
    zip_code = models.CharField(max_length=10, null=True)
    city = models.CharField(max_length=30, null=True)
    phone_number = models.CharField(max_length=30, null=True)
    contact_person_email = models.EmailField(null=True)

    def __str__(self):
        return self.title



def cv_upload_path(instance, filename):
    return 'uploads/student/{0}/cv/{1}'.format(instance.user.id, filename)



class InternType(models.Model):
    objects = Job()
    job = models.OneToOneField(Job, on_delete=models.CASCADE, null=True, related_name='interntypes')
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class JobFunction(models.Model):
    objects = Job()
    job = models.OneToOneField(Job, on_delete=models.CASCADE, null=True, related_name='jobfunctions')
    name = models.CharField(max_length=60)

    def __str__(self):
        return self.name


class JobApply(models.Model):
    objects = StudentProfile()
    objects2 =Job()
    user = models.ForeignKey(StudentProfile, on_delete=models.CASCADE)
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    motivation = models.TextField()
    upload_cv = models.FileField(upload_to=cv_upload_path, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.user.user.get_full_name() + ' > ' + self.job.title


class CurriculumVitae(models.Model):
    objects = StudentProfile()
    user = models.ForeignKey(StudentProfile, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.user.get_full_name()


class Education(models.Model):
    INSTITUTION_TYPE = [
        ('Basisschool', 'PRIMARYSCHOOL'),
        ('Middelbare school', 'HIGHSCHOOL'),
        ('Hoger beroepsonderwijs', 'UNIVERSITY1'),
        ('Universiteit', 'UNIVERSITY2')
    ]

    DEGREE_SITUATION = [
        ('Propedeuse', 'PROPEDEUSE'),
        ('Bachelor', 'BACHELOR'),
        ('Master', 'MASTER')
    ]

    objects = CurriculumVitae()
    cv = models.OneToOneField(CurriculumVitae, on_delete=models.CASCADE, related_name='educations')
    institution_type = models.CharField(max_length=30, choices=INSTITUTION_TYPE, null=True)
    institution_name = models.CharField(max_length=35, null=True, blank=True)
    degree = models.CharField(max_length=30, choices=DEGREE_SITUATION, null=True)
    field_of_study = models.CharField(max_length=35, null=True, blank=True)
    start_year = models.IntegerField(null=True, blank=True)
    end_year = models.IntegerField(null=True, blank=True)
    graduated = models.BooleanField(null=True, blank=True)



class Experience(models.Model):
    objects = CurriculumVitae()
    cv = models.OneToOneField(CurriculumVitae, on_delete=models.CASCADE, related_name='experiences')
    company_name = models.CharField(max_length=35, null=True, blank=True)
    expertise_title = models.CharField(max_length=35, null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    location = models.CharField(max_length=35, null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.cv.user.user.username


class SkillSet(models.Model):
    objects = CurriculumVitae()
    cv = models.OneToOneField(CurriculumVitae, on_delete=models.CASCADE, related_name='skillsets')
    name = models.CharField(max_length=35, null=True, blank=True )

    def __str__(self):
        return self.cv.user.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    print('****', created)
    if instance.is_student:
        StudentProfile.objects.get_or_create(user=instance)
    else:
        CompanyProfile.objects.get_or_create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    if instance.is_student:
        instance.student_profile.save()
    else:
        CompanyProfile.objects.get_or_create(user=instance)


#   Jobfunction create/save
@receiver(post_save, sender=Job)
def create_job_profile(sender, instance, created, **kwargs):
    print('****', created)
    JobFunction.objects.get_or_create(job=instance)

@receiver(post_save, sender=Job)
def save_job_profile(sender, instance, **kwargs):
    instance.jobfunctions.save()


#   Interntype create/save
@receiver(post_save, sender=Job)
def create_job_i_profile(sender, instance, created, **kwargs):
    print('****', created)
    InternType.objects.get_or_create(job=instance)

@receiver(post_save, sender=Job)
def save_job_i_profile(sender, instance, **kwargs):
    instance.interntypes.save()


#   Education create/save
@receiver(post_save, sender=CurriculumVitae)
def create_cv_education(sender, instance, created, **kwargs):
    print('****', created)
    Education.objects.get_or_create(cv=instance)

@receiver(post_save, sender=CurriculumVitae)
def save_cv_education(sender, instance, **kwargs):
    instance.educations.save()

#   Experience create/save
@receiver(post_save, sender=CurriculumVitae)
def create_cv_experience(sender, instance, created, **kwargs):
    print('****', created)
    Experience.objects.get_or_create(cv=instance)

@receiver(post_save, sender=CurriculumVitae)
def save_cv_experience(sender, instance, **kwargs):
    instance.experiences.save()

#   Skillset create/save
@receiver(post_save, sender=CurriculumVitae)
def create_cv_skillset(sender, instance, created, **kwargs):
    print('****', created)
    SkillSet.objects.get_or_create(cv=instance)

@receiver(post_save, sender=CurriculumVitae)
def save_cv_skillset(sender, instance, **kwargs):
    instance.skillsets.save()

