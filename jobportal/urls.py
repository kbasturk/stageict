"""stageict URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views
from django.conf.urls import url


urlpatterns = [
    path('', views.index, name='index'),
    path('companies', views.all_companies_view, name='all_companies'),
    path('students', views.all_students_view, name='all_students'),
    path('account/signup/student', views.student_signup_view, name='student_signup'),
    path('account/signup/company', views.company_signup_view, name='company_signup'),
    path('account/profile/student/<id>', views.student_profile_view, name='student_profile'),
    path('account/profile/student/settings/<id>', views.student_profile_settings_view, name='student_profile_settings'),
    path('account/profile/student/create-cv/', views.create_cv_view, name='create_cv'),
    path('account/profile/company/<id>', views.company_profile_view, name='company_profile'),
    path('account/profile/company/settings/<id>', views.company_profile_settings_view, name='company_profile_settings'),
    path('jobs', views.show_jobs, name='show_jobs'),
    path('jobs/job-detail/<id>', views.job_detail, name='job-detail'),
    path('account/company/job/add/', views.company_create_job, name='create_job')
]
