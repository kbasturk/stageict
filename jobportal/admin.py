from django.contrib import admin
from django.contrib.auth.models import Group
from .models import User, StudentProfile, CompanyProfile, JobFunction, InternType, Job, JobApply, Interest, CurriculumVitae, Education, Experience, SkillSet

# Unregistering models.
admin.site.unregister(Group)

# Registering models.
admin.site.register(User)
admin.site.register(StudentProfile)
admin.site.register(CompanyProfile)
admin.site.register(JobFunction)
admin.site.register(InternType)
admin.site.register(Job)
admin.site.register(JobApply)
admin.site.register(Interest)
admin.site.register(CurriculumVitae)
admin.site.register(Education)
admin.site.register(Experience)
admin.site.register(SkillSet)
