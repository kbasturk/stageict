from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from multi_form_view import MultiFormView

from .models import User, StudentProfile, CompanyProfile, Job, JobFunction, InternType, Education, Experience, SkillSet, \
    CurriculumVitae, JobApply

"""Forms
In forms.py we are building forms where we use it in the template and sending it to the view.
"""

class UserForm(forms.ModelForm):
    class Meta:
        model = User

        fields = ('username',
                  'first_name',
                  'last_name',
                  'password',
                  'email')

        labels = {
            'username': 'Gebruikersnaam',
            'first_name': 'Voornaam',
            'last_name': 'Achternaam',
            'email': 'E-mail',
        }

        help_texts = {
            'username': '',
        }     

        error_messages = {
            'username': {
                'required': '', 
            },
            'password':{
                'required': '',
            }, 
        }

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user


class StudentProfileForm(forms.ModelForm):
    class Meta:
        model = StudentProfile

        fields = ('phone_number',
                  'gender',
                  'date_of_birth',
                  'slogan',
                  'age',
                  'language'
                  )

        error_messages = {
            'gender': {
                'required': '',
            }
        }

class CompanyProfileForm(forms.ModelForm):
    class Meta:
        model = CompanyProfile

        fields = ('company_name', 'website', 'company_image', 'address', 'zip_code', 'city', 'phone_number',
                  )

        labels = {
            'company_name': 'Bedrijfsnaam',
            'company_image': 'Bedrijfslogo',
            'address': 'Straatnaam',
            'zip_code': 'Postcode',
            'city': 'Plaats',
            'phone_number': 'Telefoonnummer',
        }

        help_texts = {
            'zip_code': '1000 AA',
        }        

        error_messages = {
            'company_name': {
                'required': '', 
            },
            'website':{
                'required': '',
            }, 
            'company_image':{
                'required': '',
            },     
            'address':{
                'required': '',
            }, 
            'zip_code':{
                'required': '',
            }, 
            'city':{
                'required': '',
            }, 
            'phone_number':{
                'required': '',
            }, 
        }

class EditUserSettingsForm(UserChangeForm):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'password'
        )
        labels = {
            'first_name': 'Voornaam',
            'last_name': 'Achternaam',
            'email': 'E-mail',
        }


class EditStudentProfileSettingsForm(UserChangeForm):
    class Meta:
        model = StudentProfile

        fields = (
            'phone_number',
            'gender',
            'date_of_birth',
            'slogan',
            'language',
            'age',

        )
        labels = {
            'phone_number': 'Telefoonnummer',
            'gender': 'Geslacht',
            'date_of_birth': 'Geboortedatum',
            'slogan': 'Slogan',
            'language': 'Taal',
            'age': 'Leeftijd',
        }

class EditCompanyUserSettingsForm(UserChangeForm):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'password'
        )
        labels = {
            'first_name': 'Voornaam',
            'last_name': 'Achternaam',
            'email': 'E-mail',
        }

class EditCompanyProfileSettingsForm(UserChangeForm):
    class Meta:
        model = CompanyProfile

        fields = ('company_name',
                  'website',
                  'company_image',
                  'address',
                  'zip_code',
                  'phone_number',
                  'city',
                  )
        labels = {
            'company_name': 'Bedrijfsnaam',
            'address': 'Adres',
            'zip_code': 'Postcode',
            'phone_number': 'Telefoonnummer',
            'city': 'Stad',
            'company_image': 'Bedrijf foto'

        }

# ------------JOB--------------- #

class JobAddForm(forms.ModelForm):
    class Meta:
        model = Job

        fields = (
                  'title',
                  'description',
                  'compensation',
                  'contact_person',
                  'contact_person_image',
                  'created_at',
                  'is_active',
                  'address',
                  'zip_code',
                  'phone_number',
                  'city',
                  'contact_person_email'
                  )
        labels = {
            'description': '',
            'contact_person': 'Contactpersoon',
            'compensation': 'compensatie',
            'title': 'titel',
            'is_active': 'Vacature is actief',
            'address': 'Adres',
            'zip_code': 'Postcode',
            'phone_number': 'Telefoonnummer',
            'city': 'Stad',
            'contact_person_image': 'Foto contact persoon'

        }

    def save(self, commit=True):
        job = super(JobAddForm, self).save(commit=False)
        if commit:
            job.save()
        return job

class JobFunctionForm(forms.ModelForm):
    class Meta:
        model = JobFunction
        fields = (
           'name',
            )

        labels = {
           'name': 'Vacature functie',

            }

class JobInternTypeForm(forms.ModelForm):
    class Meta:
        model = InternType
        fields = (
           'name',
            )

        labels = {
           'name': 'Type stage',

            }

# ------------ CV -------------- #

class StudentCvForm(forms.ModelForm):
    class Meta:
        model = CurriculumVitae
        fields = (

        )

    def save(self, commit=True):
        vc = super(StudentCvForm, self).save(commit=False)
        if commit:
            vc.save()
        return vc

class StudentEducationForm(forms.ModelForm):
    class Meta:
        model = Education
        fields = (
            'institution_type',
            'institution_name',
            'degree',
            'field_of_study',
            'start_year',
            'end_year',
            'graduated'
        )
        labels = {
            'institution_type': 'Type instutitie',
            'institution_name': 'Naam instutitie',
            'degree': 'Graad',
            'field_of_study': 'Achtergrond studie',
            'start_year': 'Start jaar',
            'end_year': 'Eind jaar',
            'graduated': 'Geslaagd'
        }

class StudentExperienceForm(forms.ModelForm):
    class Meta:
        model = Experience
        fields = (
            'company_name',
            'expertise_title',
            'start_date',
            'end_date',
            'location',
            'description',
        )
        labels = {
            'company_name': 'Naam bedrijf',
            'expertise_title': 'Expertise titel',
            'start_date': 'Start jaar',
            'end_date': 'Eind jaar',
            'location': 'Locatie',
            'description': 'Werkomschrijving',
        }


class StudentSkillsetForm(forms.ModelForm):
    class Meta:
        model = SkillSet
        fields = (
            'name',
        )
        labels = {
            'name': 'Skillset/Expertise'
        }


class SendMailForm(forms.ModelForm):
    class Meta:
        model = User
        from_email = forms.EmailField(required=True)
        first_name = forms.CharField(required=True)
        last_name = forms.CharField(required=True)
        message = forms.CharField(widget=forms.Textarea, required=True)

        fields = ('first_name', 'last_name', 'email')


class JobAddForm(forms.ModelForm):
    class Meta:
        model = Job

        fields = ('user',
                  'title',
                  'description',
                  'compensation',
                  'last_date',
                  'contact_person',
                  'contact_person_image',
                  )

class ApplyForm(forms.ModelForm):
    class Meta:
        model = JobApply

        fields = ('motivation',
                  'upload_cv',
                  )

        labels = {
            'motivation': 'Motivatie',
            'upload_cv': 'Upload uw CV',
        }

class CompanyContactStudentForm(forms.Form):
    msg = forms.CharField(widget=forms.Textarea)
    msg.label = "Bericht"
