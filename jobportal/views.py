from django.contrib.auth import login, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_protect
from .models import User, StudentProfile, CompanyProfile, Education, CurriculumVitae, Experience, SkillSet, Interest, Job, JobFunction, InternType
from .forms import UserForm, SendMailForm, StudentProfileForm, EditUserSettingsForm, EditCompanyProfileSettingsForm, \
    EditStudentProfileSettingsForm, EditCompanyUserSettingsForm, JobAddForm, CompanyProfileForm, JobFunctionForm, \
    JobInternTypeForm, StudentEducationForm, StudentExperienceForm, StudentSkillsetForm, StudentCvForm, ApplyForm, CompanyContactStudentForm
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.hashers import make_password
from django.contrib.auth import update_session_auth_hash
from django.conf import settings

"""StageIct Views

The views are for functionality.
"""

# Sending the user to the homepage
# @sending statistics to the template like object counts with filtering them.
def index(request):
    companies = CompanyProfile.objects.all().order_by('-id')[0:8]
    jobs = Job.objects.all().prefetch_related('jobfunctions', 'interntypes').order_by('-id')[0:6]
    jobs_olds = Job.objects.all().prefetch_related('jobfunctions', 'interntypes').order_by('id')[0:6]
    
    #counts
    students_count = StudentProfile.objects.all().count()
    companies_count = CompanyProfile.objects.all().count()
    jobs_count = Job.objects.all().count()


    context = {
        'companies': companies,
        'jobs': jobs,
        'jobs_olds': jobs_olds,
        'students_count': students_count,
        'companies_count': companies_count,
        'jobs_count': jobs_count,
    }

    return render(request, 'jobportal/index.html', context, {

    })


# @cache_page(60 * 15) Using this in live version to enable caching.
# Student signup: @saving the student by using the form UserForm in Forms
@csrf_protect
def student_signup_view(request):
    user_form = UserForm(request.POST, prefix='UF')

    if request.method == 'POST':

        if user_form.is_valid():
            user = user_form.save(commit=False)
            user.password = make_password(user.password)
            user.is_student = True
            user.save()

            user.student_profile.save()

        else:
            user_form = UserForm(prefix='UF')

    return render(request, 'jobportal/account/student_signup.html', {
        'user_form': user_form,
    })

@csrf_protect # Protects to unwanted action of the browser/user.
#Saving the companyprofile with additional data getting by the POST and saving it in the database.
def company_signup_view(request):

    user_form = UserForm(request.POST or None, prefix='UF')
    profile_form = CompanyProfileForm(request.POST or None, prefix='PF')

    if request.method == 'POST':
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save(commit=False)
            user.password = make_password(user.password)
            user.is_company = True
            user.save()

            user.company_profile.company_name = profile_form.cleaned_data.get('company_name')
            user.company_profile.company_image = profile_form.cleaned_data.get('company_image')
            user.company_profile.website = profile_form.cleaned_data.get('website')
            user.company_profile.address = profile_form.cleaned_data.get('address')
            user.company_profile.zip_code = profile_form.cleaned_data.get('zip_code')
            user.company_profile.city = profile_form.cleaned_data.get('city')
            user.company_profile.phone_number = profile_form.cleaned_data.get('phone_number')
            user.company_profile.save()

        else:
                user_form = UserForm(prefix='UF')
                profile_form = CompanyProfileForm(prefix='PF')


    return render(request, 'jobportal/account/company_signup.html',{
        'user_form': user_form,
        'profile_form': profile_form,
    })

# Saving the updates of the StudentProfile by showing and saving the form.
def student_profile_settings_view(request, id):
    user1 = User.objects.get(pk=id)
    student = StudentProfile.objects.get(user=user1)
    form = EditUserSettingsForm(request.POST, instance=user1)
    profile_form = EditStudentProfileSettingsForm(request.POST or None, instance=student)

    if request.method == 'POST':

        if form.is_valid():
            form.save()
            profile_form.save()

        else:
            form = UserForm(prefix='UF')

    if request.method == 'POST':

        if profile_form.is_valid():

            profile_form.save()
        else:

            profile_form = EditStudentProfileSettingsForm(prefix='PF')

    context = {
        'user': user1,
        'form': form,
        'profile_form': profile_form,
    }

    return render(request, 'jobportal/account/student_profile_settings.html', context, {
    })


#Rendering the Student Profile by sending data to the template where it can be shown to the user.
def student_profile_view(request, id):
    user1 = User.objects.get(pk=id)
    student = StudentProfile.objects.get(user=user1)

    try:
        cur = CurriculumVitae.objects.get(user=student)
        education = Education.objects.filter(cv=cur)
        experience = Experience.objects.filter(cv=cur)
        skillset = SkillSet.objects.filter(cv=cur)
        interest = Interest.objects.filter(user=student)  # Filter omdat er meerdere objecten zijn.

        lastlogin = user1.last_login
        context = {
            'user': user1,
            'student': student,
            'cv' : cur,
            'education': education,
            'experience': experience,
            'skillset': skillset,
            'interest': interest,
            'lastlogin': lastlogin,
        }

    except:
        lastlogin = user1.last_login
        context = {
        'user': user1,
        'student': student,
        'lastlogin': lastlogin,
        }

    return render(request, 'jobportal/account/student_profile.html', context, {
    })

#Rendering the Company Profile by sending data to the template where it can be shown to the user.
def company_profile_view(request, id):
    user1 = User.objects.get(pk=id)
    company = CompanyProfile.objects.get(user=user1)
    job = Job.objects.filter(user=company).order_by('-is_active')
    job_count = Job.objects.filter(user=company).count()
    job_active_count = Job.objects.filter(user=company).filter(is_active = True).values('is_active').count()
    job_inactive_count = Job.objects.filter(user=company).filter(is_active = False).values('is_active').count()


    context = {
        'user': user1,
        'company': company,
        'job': job,
        'job_count': job_count,
        'job_active_count': job_active_count,
        'job_inactive_count': job_inactive_count
    }

    return render(request, 'jobportal/account/company_profile.html', context, {
    })

# This function is for saving/updating the Company Profile by rendering a form of the current information of the Company.
def company_profile_settings_view(request, id):
    user1 = User.objects.get(pk=id)
    company = CompanyProfile.objects.get(user=user1)
    form = EditCompanyUserSettingsForm(request.POST, instance=user1)
    profile_form = EditCompanyProfileSettingsForm(request.POST or None, instance=company)

    # Form save for usersettings
    if request.method == 'POST':
        if form.is_valid():
            form.save()
        else:
            form = UserForm(prefix='UF')

    # Save form for profilesettings
    if request.method == 'POST':
        if profile_form.is_valid():
            profile_form.save()
        else:
            profile_form = EditCompanyProfileSettingsForm(prefix='PF')

    context = {
        'user': user1,
        'company': company,
        'form': form,
        'profile_form': profile_form,
        'url_id': id
    }

    return render(request, 'jobportal/account/company_profile_settings.html', context, {
    })

#This function is to add Job's as a Company.
def company_add_job(request, id):
    user1 = User.objects.get(pk=id)
    add_job_form = JobAddForm(request.POST, prefix='PF')
    if request.method == 'POST':

        if add_job_form.is_valid():
            addJob = add_job_form.save(commit=False)
            add_job_form.save()

        else:
            addJob = JobAddForm(prefix='UF')

    context = {
        'user': user1,
    }

    return render(request, 'jobportal/company/create_job.html', {
        'add_job_form': add_job_form,
    })


#---------------------------JOB---------------------------#

def company_create_job(request):
    user1 = User.objects.get(pk=request.user.id)  # Get instance
    company = CompanyProfile.objects.get(user=user1)

    add_job_form = JobAddForm(request.POST or None)
    add_jobfunction_form = JobFunctionForm(request.POST or None)
    add_jobtype_form = JobInternTypeForm(request.POST or None)


    # Checking if the form is valid and if its a POST method then continue:
    if request.method == 'POST':

        if add_job_form.is_valid() and add_jobfunction_form.is_valid() and add_jobtype_form.is_valid():
            jobf = add_job_form.save(commit=False)
            jobf.user = company
            jobf.title = add_job_form.cleaned_data.get('title')
            jobf.description = add_job_form.cleaned_data.get('description')
            jobf.compensation = add_job_form.cleaned_data.get('compensation')
            jobf.contact_person = add_job_form.cleaned_data.get('contact_person')
            jobf.is_active = add_job_form.cleaned_data.get('is_active')
            jobf.address = add_job_form.cleaned_data.get('address')
            jobf.zip_code = add_job_form.cleaned_data.get('zip_code')
            jobf.phone_number = add_job_form.cleaned_data.get('phone_number')
            jobf.city = add_job_form.cleaned_data.get('city')
            jobf.contact_person_email = add_job_form.cleaned_data.get('contact_person_email')
            jobf.save()

            jobf.jobfunctions.name = add_jobfunction_form.cleaned_data.get('name')
            jobf.jobfunctions.save()

            jobf.interntypes.name = add_jobtype_form.cleaned_data.get('name')
            jobf.interntypes.save()



        else:
            add_job_form = JobAddForm(prefix='UF')
            add_jobfunction_form = JobInternTypeForm(prefix='UF')
            add_jobtype_form = JobFunctionForm(prefix='UF')

    context = {
        'user': user1,
        'company': company,
        'add_job_form': add_job_form,
        'add_jobfunction_form': add_jobfunction_form,
        'add_jobtype_form': add_jobtype_form,
    }

    return render(request, 'jobportal/company/create_job.html', context, {
    })

def create_cv_view(request):
    user1 = User.objects.get(pk=request.user.id)  # Haal instance op
    student = StudentProfile.objects.get(user=user1)

    cv_form = StudentCvForm(request.POST or None)
    add_education_form = StudentEducationForm(request.POST or None)
    add_experience_form = StudentExperienceForm(request.POST or None)
    add_skillset_form = StudentSkillsetForm(request.POST or None)

    if request.method == 'POST':

        if cv_form.is_valid() and add_education_form.is_valid():

            cvf = cv_form.save(commit=False)
            cvf.user = student
            cvf.save()

            cvf.educations.institution_type = add_education_form.cleaned_data.get('institution_type')
            cvf.educations.institution_name = add_education_form.cleaned_data.get('institution_name')
            cvf.educations.degree = add_education_form.cleaned_data.get('degree')
            cvf.educations.field_of_study = add_education_form.cleaned_data.get('field_of_study')
            cvf.educations.start_year = add_education_form.cleaned_data.get('start_year')
            cvf.educations.end_year = add_education_form.cleaned_data.get('end_year')
            cvf.educations.graduated = add_education_form.cleaned_data.get('graduated')
            cvf.educations.save()
            # print("*********************************")
            # print(cvf.educations.institution_type)

            # cvf.experiences.company_name = add_experience_form.get('company_name')
            # cvf.experiences.expertise_title = add_experience_form.cleaned_data.get('expertise_title')
            # cvf.experiences.start_date = add_experience_form.cleaned_data.get('start_date')
            # cvf.experiences.end_date = add_experience_form.cleaned_data.get('end_date')
            # cvf.experiences.location = add_experience_form.cleaned_data.get('location')
            # cvf.experiences.description = add_experience_form.cleaned_data.get('description')
            # cvf.experiences.save()

            cvf.skillsets.name = add_skillset_form.cleaned_data.get('name')
            cvf.experiences.save()

        else:
            add_education_form = StudentEducationForm(prefix='UF')
            add_experience_form = StudentExperienceForm(prefix='UF')
            add_skillset_form = StudentSkillsetForm(prefix='UF')

    context = {
        'user': user1,
        'student': student,
        'add_education_form': add_education_form,
        'add_experience_form': add_experience_form,
        'add_skillset_form': add_skillset_form,
        'cv_form': cv_form



    }

    return render(request, 'jobportal/account/create_cv.html', context, {

    })


def email_send_view(request):
    if request.method == 'GET':
        form = SendMailForm()
    else:
        form = SendMailForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            try:
                send_mail(first_name, last_name, message, from_email, ['admin@example.com'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return redirect('success')
    return render(request, 'jobportal/account/student_profile.html', {'form': form})
    


def successView(request):
    return HttpResponse('Success! Thank you for your message.')

#Return all companies
def all_companies_view(request):
    companies = CompanyProfile.objects.all()

    context = {
        'companies': companies,
    }

    return render(request, 'jobportal/companies.html', context, {

    })

#Return all students
def all_students_view(request):
    students = StudentProfile.objects.all()

    context = {
        'students': students,
    }

    return render(request, 'jobportal/students.html', context, {

    })   

#Return all jobs
# @Prefetching the JobFunction and InternType model because both are ForeignKey's to the Job model
# @send_mail Sending e-mail with SMTP (settings.py). This is for applying for a Job.     
def show_jobs(request):
    jobs = Job.objects.all().prefetch_related('jobfunctions', 'interntypes')
    jobs_count = Job.objects.all().count()

    form = ApplyForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():

            user = User.objects.get(pk=request.user.pk)
            student = StudentProfile.objects.get(user=user)
            student_email = student.user.email

            data = form.save(commit=False)
            data.user = student
            data.job = Job.objects.get(pk=1)
            data.save()

            subject = "Sollicitatie"
            message = data.motivation
            mail = settings.EMAIL_HOST_USER
            reciver = ["stage_ict@hotmail.com"]
            send_mail(subject, message, mail, reciver, fail_silently=False)

    else:
        form = ApplyForm()


    context = {
       'jobs': jobs,
       'form': form,
       'jobs_count': jobs_count,
    }

    return render(request, 'jobportal/jobs/browse-job.html', context, {
    })

#Sending all detail information of the Job by finding it with a GET request where it checks the link and comparing it to the Job and Company ID.
def job_detail(request, id):
    job = Job.objects.get(pk=id)
    job_company = job.user.id
    company = CompanyProfile.objects.get(id=job_company)
    job_function = JobFunction.objects.get(job=job)
    intern_type = InternType.objects.get(job=job)
    related_jobs = Job.objects.filter(user=company).exclude(id=id).order_by('-id')[0:4].prefetch_related('jobfunctions', 'interntypes')
    
    form = ApplyForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():

            user = User.objects.get(pk=request.user.pk)
            student = StudentProfile.objects.get(user=user)
            student_email = student.user.email #todo
            
            data = form.save(commit=False)
            data.user = student
            data.job = job
            data.save()

            subject = "Sollicitatie van: " + student_email
            message = data.motivation
            mail = settings.EMAIL_HOST_USER
            reciever = [job.contact_person_email]
            send_mail(subject, message, mail, reciever, fail_silently=False)

    context = {
        'job': job,
        'job_function': job_function,
        'intern_type': intern_type,
        'related_jobs': related_jobs,
        'form': form,
    }

    return render(request, 'jobportal/jobs/job-detail.html', context, {

    })
